// audioPlayerClass
// rohitroy

class AudioPlayerClass {
  constructor(songs) {
    this.songs = songs;
    this.songIndex = 0;
    this.musicContainer = document.getElementById("music-container");
    this.playBtn = document.getElementById("play");
    this.prevBtn = document.getElementById("prev");
    this.nextBtn = document.getElementById("next");
    this.progressContainer = document.getElementById("progress-container");
    this.audio = document.getElementById("audio");
    this.progress = document.getElementById("progress");
    this.title = document.getElementById("title");
    this.cover = document.getElementById("cover");
    // this.currTime = document.querySelector("#currTime");
    // this.durTime = document.querySelector("#durTime");
    this.eventListener();

    // InitialloadSong
    this.loadSong();
  }

  eventListener() {
    this.playBtn.addEventListener("click", () => {
      const isPlaying = this.musicContainer.classList.contains("play");
      if (isPlaying) {
        this.pauseSong();
      } else {
        this.playSong();
      }
    });

    // this.playBtn.addEventListener("click", () =>
    //   this.playPauseCheck.bind(this)
    // );
    this.prevBtn.addEventListener("click", this.prevSong.bind(this));
    this.nextBtn.addEventListener("click", this.nextSong.bind(this));
    this.audio.addEventListener("timeupdate", this.updateProgress.bind(this));
    this.progressContainer.addEventListener(
      "click",
      this.setProgress.bind(this)
    );
    this.audio.addEventListener("ended", this.nextSong.bind(this));
  }

  // functionToLoadAParticularSong

  // playPauseCheck() {
  //   const isPlaying = this.musicContainer.classList.contains("play");
  //   if (isPlaying) {
  //     this.pauseSong();
  //   } else {
  //     this.playSong();
  //   }
  // }

  loadSong(song = this.songs[this.songIndex]) {
    this.title.innerText = song;
    this.audio.src = `./user/entity/AudioPlayer/songs/${song}.mp3`;
    this.cover.src = `./user/entity/AudioPlayer/images/${song}.jpg`;
  }

  //   functionToPlaySong
  playSong() {
    // addingSpinningAnimation
    this.musicContainer.classList.add("play");

    // togglingButton
    this.playBtn.querySelector("i.fas").classList.remove("fa-play");
    this.playBtn.querySelector("i.fas").classList.add("fa-pause");
    // this.loadSong(this.songs[songIndex]);

    this.audio.play();
  }

  //   functionToPauseSong
  pauseSong() {
    // stoppingSpinningAnimation
    this.musicContainer.classList.remove("play");

    // togglingButton
    this.playBtn.querySelector("i.fas").classList.add("fa-play");
    this.playBtn.querySelector("i.fas").classList.remove("fa-pause");

    this.audio.pause();
  }

  // functionForPlayingPreviousSong
  prevSong() {
    this.songIndex--;

    if (this.songIndex < 0) {
      this.songIndex = this.songs.length - 1;
    }

    this.loadSong(this.songs[this.songIndex]);

    this.playSong();
  }

  // functionForPlayingNextSong
  nextSong() {
    this.songIndex++;

    if (this.songIndex > this.songs.length - 1) {
      this.songIndex = 0;
    }

    this.loadSong(this.songs[this.songIndex]);

    console.log(this.songIndex - 1);

    this.playSong();
  }

  // functionForUpdatingProgressBar
  updateProgress(e) {
    const {duration, currentTime} = e.srcElement;
    const progressPercent = (currentTime / duration) * 100;
    progress.style.width = `${progressPercent}%`;
  }

  // functionSetProgressBar
  setProgress(e) {
    const width = this.clientWidth;
    const clickX = e.offsetX;
    const duration = audio.duration;

    this.audio.currentTime = (clickX / width) * duration;
  }
}

export {AudioPlayerClass};
