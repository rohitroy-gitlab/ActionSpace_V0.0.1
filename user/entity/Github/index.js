const GetGithub = {
  scope: "Browser",
  name: "Github",
  path: "^#GetGithub$",
  entity: "Ehh",
  model: {
    data: "",
  },
  view(model) {
    var container = document.getElementById("ehh");
    container.innerHTML = `
    <form id="search-form" class="search-form">
      <input type="text" name="repoName" placeholder="Enter repository name">
      <button type="submit">Search</button>
    </form>
      <div id="results"></div>
    `;
    var results = document.getElementById("results");
    results.innerHTML = ``;
    if (model.data) {
      for (let repo of model.data) {
        const div = document.createElement("div");
        div.classList.add("repo");
        div.innerHTML = `
                      <h3><a target="_blank" href="${repo.html_url}">${repo.name}</a></h3>
                      <p>${repo.description}</p>
                  `;
        results.appendChild(div);
      }
    }
  },
  controller: {
    name: "ExplorerPage",
    callback: async (event, view, model, callback) => {
      view(model);

      document
        .getElementById("search-form")
        .addEventListener("submit", async (event) => {
          event.preventDefault();
          console.log(event);
          const repoName = event.target[0].value;
          const url = `https://api.github.com/search/repositories?q=${repoName}`;
          await fetch(url, {
            headers: {
              Authorization:
                "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",
            },
          })
            .then((response) => response.json())
            .then((data) => {
              model.data = data.items;
            });
          await callback(event, view, model, callback);
        });
    },
  },
};
const Github = {
  GetGithub,
};
export {Github};
