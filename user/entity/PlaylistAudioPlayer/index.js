import {PlaylistAudioPlayerClass} from "../../plugin/PlaylistAudioPlayerClass.js";

const GetPlaylistAudioPlayer = {
  scope: "Browser",
  name: "PlaylistAudioPlayer",
  path: "^#GetPlaylistAudioPlayer$",
  entity: "Ehh",
  model: {
    // DeclareSongArray
    songs: [],
  },
  view(model) {
    var container = document.getElementById("ehh");
    container.innerHTML = `
    <div class="container">
    <div class="playlistAudioPlayerContainer">
    <audio id="audioElement"></audio>
    <input type="file" id="fileInput" class="input-btn" multiple />
    <div id="songDisplay" class="song-display-container"></div>
    <div class="prev-next-btns">
      <button id="prevBtn" class="play-btn"><<< Prev</button>
      <button id="nextBtn" class="pause-btn">Next >>></button>
    </div>
    <div class="play-pause-btns">
      <button id="playBtn" class="play-btn">Play</button>
      <button id="pauseBtn" class="pause-btn">Pause</button>
    </div>
    <button id="resetBtn" class="reset-btn">Reset</button>
  </div></div>
    `;
    let PlaylistAudioPlayerClassAB = new PlaylistAudioPlayerClass(model.songs);
  },
  controller: {
    name: "PlaylistAudioPlayerPage",
    callback: async (event, view, model, callback) => {
      view(model);

      // // addTheClass
      // let AudioPlayerAB = new AudioPlayer(model.songs);
      // AudioPlayerAB.loadSong(model.songs);
      // // loadSong(songs[songIndex]);
      // await callback(event, view, model, callback);
    },
  },
};
const PlaylistAudioPlayer = {
  GetPlaylistAudioPlayer,
};
export {PlaylistAudioPlayer};
