import {AudioPlayerClass} from "../../plugin/AudioPlayerClass.js";
// AudioEditorEntityCodes
const GetAudioPlayer = {
  scope: "Browser",
  name: "AudioPlayer",
  path: "^#GetAudioPlayer$",
  entity: "Ehh",
  model: {
    // trainingData
    songs: ["song-1", "song-2", "song-3"],
  },
  view(model) {
    var container = document.getElementById("ehh");
    container.innerHTML = `
    <div class="container">
    <div class="music-container" id="music-container">
      <div class="heading">
      <h2 id="title"></h2>
      </div>

      <div class="img-container">
        <img src='./user/entity/AudioEditor/images/${model.songs[0]}.jpg' alt="music-cover" id="cover" />
      </div>
      <audio src="./user/entity/AudioEditor/songs/${model.songs[0]}.mp3" id="audio"></audio>

      <div class="music-info">
        <div class="progress-container" id="progress-container">
          <div class="progress" id="progress"></div>
        </div>
      </div>

      <div class="navigation">
        <button id="prev" class="action-btn">
          <i class="fas fa-backward"></i>
        </button>
        <button id="play" class="action-btn action-btn-big">
          <i class="fas fa-play"></i>
        </button>
        <button id="next" class="action-btn">
          <i class="fas fa-forward"></i>
        </button>
      </div>
    </div>
    </div>
      `;
    let AudioPlayerClassAB = new AudioPlayerClass(model.songs);
  },

  controller: {
    name: "AudioPlayerPage",
    callback: async (event, view, model, callback) => {
      view(model);

      // // addTheClass
      // let AudioPlayerAB = new AudioPlayer(model.songs);
      // AudioPlayerAB.loadSong(model.songs);
      // // loadSong(songs[songIndex]);
      // await callback(event, view, model, callback);
    },
  },
};
const AudioPlayer = {
  GetAudioPlayer,
};
export {AudioPlayer};
